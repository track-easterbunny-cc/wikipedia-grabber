# track.easterbunny.cc Wikipedia Grabber Changelog
This is the changelog for the track.easterbunny.cc Wikipedia Grabber. It details all the changes made in releases of the wikikpedia grabber.

# Version 1.0.0 (track.easterbunny.cc Version 5.5.1)
* The wikipedia grabber is now open source!
