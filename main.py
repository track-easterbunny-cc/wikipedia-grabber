# track.easterbunny.cc Wikipedia Grabber v1.0.0
# Copyright (C) 2021 track.easterbunny.cc

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import csv
import requests
import re2 as re

wikipedia_file = open("wikipedia.tsv", "w")

excluded_stops = ["Debrecen", "S%C3%A3o_Paulo", "Buenos_Aires", "Quebec_City", "Al_Ain"]

sentence_limit = "4"

wikipedia_tsv = csv.writer(wikipedia_file, delimiter="\t", quotechar='"')
with open("route.tsv") as fl:
    length = sum(1 for line in fl)

with open("route.tsv") as fc:
    rd = csv.reader(fc, delimiter="\t", quotechar='"')
    index = 0
    tempcont = 0
    for row in rd:
        if tempcont == 0:
            wikipedia_tsv.writerow([""])
            tempcont = 1
            index = index + 1
            continue

        link = row[17]
        if link == "":
            wikipedia_tsv.writerow([""])
            index = index + 1
            continue

        wikipedia_title = link.split("/")[-1]
        print("Working on row " + str(index) + ", Wikipedia title: " + wikipedia_title)
        wikipedia_request = requests.get("https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&titles=%s&formatversion=2&exsentences=%s&exlimit=1&exintro=1&explaintext=1" % (wikipedia_title, sentence_limit))
        wikipedia_json = wikipedia_request.json()
        try:
            wikipedia_descr = wikipedia_json['query']['pages'][0]['extract']
        except KeyError:
            pass
            index = index + 1
        # Parse
        wikipedia_descr = wikipedia_descr.replace("\n", "")
        if wikipedia_title in excluded_stops:
            print("Stop was in whitelist for excluded stops. Only doing basic filtering.")
            wikidescr_2 = wikipedia_descr
            wikidescr_2 = wikidescr_2.replace("listen", "")
            wikidescr_2 = wikidescr_2.replace("U. S.", "U.S.")
            wikidescr_2 = wikidescr_2.replace("D. C.", "D.C.")
            wikidescr_2 = wikidescr_2.replace("A. C.", "A.C.")
            wikidescr_2 = wikidescr_2.replace("  ", " ")
            wikidescr_2 = wikidescr_2.replace(" , ", ", ")
            wikidescr_2 = wikidescr_2.replace(" . ", ". ")
            wikidescr_2 = wikidescr_2.replace(" ", "")
            wikidescr_2 = wikidescr_2.replace("()", "")
            wikidescr_2 = wikidescr_2.replace("( )", "")
            wikidescr_2 = wikidescr_2.replace(" .", ".")
            wikidescr_2 = wikidescr_2.replace("  ", " ")
            wikidescr_2 = wikidescr_2.replace(" ,", ",")
            wikidescr_2 = wikidescr_2.replace("( ", "(")
            wikidescr_2 = wikidescr_2.replace("(,", "(")
            wikipedia_tsv.writerow([wikidescr_2])
            index = index + 1
            continue

        wikipedia_descr = wikipedia_descr.replace("(listen)", "listen")
        matches = re.findall('\((?<=\()(?:[^()]*|\([^)]*\))*\)', wikipedia_descr)
        wikidescr_2 = wikipedia_descr
        try:
            for match in matches:
                if match.find(":") != -1 or match.find(";") != -1 or match.find("[") != -1 or match.find("]") != -1 or match.find("listen") != -1 or match.find("pronounced") != -1 or match.find("pronunciation") != -1:
                    wikidescr_2 = wikidescr_2.replace(match, "")
        except TypeError:
            print("Error with regex matches.")
        wikidescr_2 = re.sub(r"([.])([A-Z])", r"\1 \2", wikidescr_2)
        wikidescr_2 = wikidescr_2.replace("listen", "")
        wikidescr_2 = wikidescr_2.replace("U. S.", "U.S.")
        wikidescr_2 = wikidescr_2.replace("D. C.", "D.C.")
        wikidescr_2 = wikidescr_2.replace("A. C.", "A.C.")
        wikidescr_2 = wikidescr_2.replace("  ", " ")
        wikidescr_2 = wikidescr_2.replace(" , ", ", ")
        wikidescr_2 = wikidescr_2.replace(" . ", ". ")
        wikidescr_2 = wikidescr_2.replace(" ", "")
        wikidescr_2 = wikidescr_2.replace("()", "")
        wikidescr_2 = wikidescr_2.replace("( )", "")
        wikidescr_2 = wikidescr_2.replace(" .", ".")
        wikidescr_2 = wikidescr_2.replace("  ", " ")
        wikidescr_2 = wikidescr_2.replace(" ,", ",")
        wikidescr_2 = wikidescr_2.replace("( ", "(")
        wikidescr_2 = wikidescr_2.replace("(,", "(")
        wikipedia_tsv.writerow([wikidescr_2])
        index = index + 1

print("Done!")
