# Note: Repository deprecated
The Wikipedia grabber repository has been deprecated, as this script has been wrapped into the route compiler that is now built into the TEBCC main repository.

Nonetheless, this code will remain online for anyone who wants to use it.

# Wikipedia Grabber

Script to grab descriptions from Wikipedia with a TEBCC route file, and do most filtering that we desired to do in production (removing the pronunciation basically)

# Getting started
To use the Wikipedia Grabber, you'll need:
* A v5-style route file in TSV format, with the wikipedia source field filled out (and linking properly!)
* Python 3
* The re2/requests library (`pip3 install re2 requests`).

## Quick note on the re2 library
re2 really isn't needed to do the wikipedia parsing. We switched to re2 because we were hitting regex infinite loops, and of course re2 didn't handle those any better.

You can alternatively switch to normal re (`pip3 install re`), and switch line 3 of main.py to `import re`.

Download this repo using Git. In the same directory, you'll want to have a file named `route.tsv` that's your v5-style route.

# Usage
To use, just run main.py. The fetch rate is about 100 stops/minute. This depends heavily on your internet connection (latency being the biggest factor), if Wikipedia has cached the page or not, etc.

The regex/filtering implementation included will work on about 99% of stop descriptions but has the tendancy to get stuck in a loop. 

If the script is stuck on a stop for 10 or more seconds, follow this procedure:
* The console output will have the latest `Working on row xyz, Wikipedia title: abc`. This is printed before any fetching/regex parsing.
* On line 7 of `main.py`, add the wikipedia title into the `excluded_stops` array.
* Rerun the script.

Repeat this for any additional stops that enter a regex loop.

Once complete, a file named `wikipedia.tsv` will be outputted. Import that file into software like Google Sheets/Excel, then copy & paste the contents of the one column from `wikipedia.tsv` into your main route file.

This script works pretty well to filter out data, but you'll definitely need to do a last pass manually.

It's important to note that this script **does not** handle filtering out references, notes, etc. This is done in the route compiler.

# Customizing the script
If you want to have more control over Wikipedia descriptions, there's some stuff you can change.

Line 9 contains a `sentence_limit` variable, which tells the Wikipedia API to only return 4 sentences of the title. For us, 4 sentences was a good mix between size and having enough content to read.

We encourage you to do some research on the Wikipedia API that we're using so you can customize it for your specific needs.

If you're looking to do filtering on different areas of the description, you might want to change the regex being used. 

# Licensing
The Wikipedia Grabber is licensed under the MIT License.

# Contributing
We welcome improvements to the Wikipedia Grabber. It's definitely rough as it's a new tool for v5 - and we've love some help cleaning it up. Fork the repo, make some changes, and make a PR (or you can propose changes in an issue).
